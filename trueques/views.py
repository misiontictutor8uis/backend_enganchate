
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import UsuarioSerializer, PerfilSerializer, PublicacionPrendaSerializer
from .models import Usuario, Perfil, PublicacionPrenda

# Create your views here.
@api_view(['GET', 'POST'])
def usuarios(request):
    """Obtiene o crea usuarios"""
    if request.method == 'GET':
        lista_de_usuarios = Usuario.objects.all()
        usuariosSer = UsuarioSerializer(lista_de_usuarios, many=True)
        return Response(usuariosSer.data)
    else:
        usuariosSer = UsuarioSerializer(data=request.data)
        if usuariosSer.is_valid():
            usuariosSer.save()
            return Response(usuariosSer.data, status=status.HTTP_201_CREATED)
        return Response(usuariosSer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def perfiles(request):
    """Obtiene o crea perfiles??????"""
    if request.method == 'GET':
        lista_de_perfiles = Perfil.objects.all()
        perfilesSer = PerfilSerializer(lista_de_perfiles, many=True)
        return Response(perfilesSer.data)
    else:
        perfilesSer = PerfilSerializer(data=request.data)
        if perfilesSer.is_valid():
            perfilesSer.save()
            return Response(perfilesSer.data, status=status.HTTP_201_CREATED)
        return Response(perfilesSer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def publicaciones_prendas(request):
    """Obtiene o crea publicaciones de prendas"""
    if request.method == 'GET':
        lista_de_publicaciones = PublicacionPrenda.objects.all()
        publicaciones_prendasSer = PublicacionPrendaSerializer(lista_de_publicaciones, many=True)
        return Response(publicaciones_prendasSer.data)
    else:
        publicaciones_prendasSer = PublicacionPrendaSerializer(data=request.data)
        if publicaciones_prendasSer.is_valid():
            publicaciones_prendasSer.save()
            return Response(publicaciones_prendasSer.data, status=status.HTTP_201_CREATED)
        return Response(publicaciones_prendasSer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def actualizar_usuario(request, id):
    """Obtiene, actualiza o elimina atributos de la clase Usuario"""
    try:
        snippet = Usuario.objects.get(pk=id)
    except Usuario.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UsuarioSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UsuarioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def actualizar_perfil(request, id):
    """Obtiene, actualiza o elimina atributos de la clase Perfil"""
    try:
        snippet = Perfil.objects.get(pk=id)
    except Perfil.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PerfilSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PerfilSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def actualizar_publicacion(request, id):
    """Obtiene, actualiza o elimina atributos de la clase PublicacionPrenda"""
    try:
        snippet = PublicacionPrenda.objects.get(pk=id)
    except PublicacionPrenda.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PublicacionPrendaSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PublicacionPrendaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
