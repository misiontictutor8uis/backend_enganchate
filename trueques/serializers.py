from rest_framework import serializers
from .models import Usuario, Perfil, PublicacionPrenda

class UsuarioSerializer(serializers.ModelSerializer):
    """Serializador de la clase Usuario"""
    class Meta:
        model = Usuario
        fields = '__all__'


class PerfilSerializer(serializers.ModelSerializer):
    """Serializador de la clase Perfil"""
    class Meta:
        model = Perfil
        fields = '__all__'


class PublicacionPrendaSerializer(serializers.ModelSerializer):
    """Serializador de la clase PublicacionPrenda"""
    class Meta:
        model = PublicacionPrenda
        fields = '__all__'
