from django.contrib import admin
from .models import Perfil, PublicacionPrenda, Usuario

# Register your models here.

admin.site.register(Usuario)
admin.site.register(Perfil)
admin.site.register(PublicacionPrenda)
