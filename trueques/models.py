from django.db import models


def user_directory_path(instance, filename):

    return 'user_{0}/{1}'.format(instance.user.id, filename)


# Create your models here.
class Usuario(models.Model):
    """Esta clase representa los usuarios en el proyecto"""

    nombre = models.CharField(max_length=50)
    username = models.CharField(max_length=20)
    telefono = models.IntegerField()
    email = models.EmailField()
    contrasena = models.CharField(max_length=25)


class Perfil(models.Model):
    """Esta clase modela los perfiles de los usuarios"""

    descripcion_perfil = models.CharField(max_length=600)
    calificacion = models.FloatField()
    numero_intercambios = models.IntegerField()
    numero_prendas_publicadas = models.IntegerField()
    nombre_imagen = models.CharField(max_length=50)
    imagen_perfil = models.ImageField(upload_to='trueques/static/perfiles')


class PublicacionPrenda(models.Model):
    """Esta clase modela las prendas de los usuarios"""

    fecha_de_la_publicacion = models.IntegerField()
    lugar_de_la_publicacion = models.CharField(max_length=25)
    numero_fotos = models.IntegerField()
    descripcion_prenda = models.CharField(max_length=300)
    categoria_prenda = models.CharField(max_length=200)
    like = models.IntegerField()
    dislike = models.IntegerField()
    imagen_publicacion = models.ImageField(upload_to='trueques/static/publicacionesprendas')
